
(if (not (package-installed-p 'php-mode))
    (progn
      (package-refresh-contents)
      (package-install 'php-mode)))

(require 'php-mode)

(if (not (package-installed-p 'ac-php))
    (progn
      (package-refresh-contents)
      (package-install 'ac-php)))

(require 'ac-php)

(if (not (package-installed-p 'company-php))
    (progn
      (package-refresh-contents)
      (package-install 'company-php)))

(require 'company-php)


;(if (not (package-installed-p 'flymake-php))
;    (progn
;      (package-refresh-contents)
;      (package-install 'flymake-php)))
;
;(require 'flymake-php)
;(add-hook 'php-mode-hook 'flymake-php-load)
