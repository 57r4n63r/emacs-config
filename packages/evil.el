
(if (not (package-installed-p 'evil))
    (progn
      (package-refresh-contents)
      (package-install 'evil)))

(require 'evil)

(if (not (package-installed-p 'evil-surround))
    (progn
      (package-refresh-contents)
      (package-install 'evil-surround)))

(require 'evil-surround)

(if (not (package-installed-p 'evil-numbers))
    (progn
      (package-refresh-contents)
      (package-install 'evil-numbers)))

(require 'evil-numbers)

(if (not (package-installed-p 'evil-numbers))
    (progn
      (package-refresh-contents)
      (package-install 'evil-numbers)))

(require 'evil-numbers)

(if (not (package-installed-p 'evil-commentary))
    (progn
      (package-refresh-contents)
      (package-install 'evil-commentary)))

(require 'evil-commentary)

(evil-mode 1)

(global-evil-surround-mode 1)

(require 'evil-numbers)

(evil-commentary-mode)
