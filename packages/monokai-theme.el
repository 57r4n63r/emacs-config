
(if (not (package-installed-p 'monokai-theme))
    (progn
      (package-refresh-contents)
      (package-install 'monokai-theme)))

(require 'monokai-theme)

(load-theme 'monokai t)
