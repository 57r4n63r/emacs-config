(if (not (package-installed-p 'neotree))
    (progn
      (package-refresh-contents)
      (package-install 'neotree)))

(require 'neotree)
