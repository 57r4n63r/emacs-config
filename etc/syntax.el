; Enable flymake syntax checking
(if (not (package-installed-p 'flycheck))
    (progn
      (package-refresh-contents)
      (package-install 'flycheck)))

(require 'flycheck)

(add-hook 'after-init-hook #'global-flycheck-mode)

; Show flycheck errors buffer on syntax error
(add-hook 'flycheck-after-syntax-check-hook
          (lambda  ()
            (if flycheck-current-errors
                (flycheck-list-errors)
              (when (get-buffer "*Flycheck errors*")
                (switch-to-buffer "*Flycheck errors*")
                (kill-buffer (current-buffer))
                (delete-window)))))
