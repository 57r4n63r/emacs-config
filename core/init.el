
(defconst core-directory (concat user-emacs-directory "core/")
          "Directory containing other configs")

(defconst packages-directory (concat user-emacs-directory "packages/")
          "Directory containing all packages configurations")

(defconst languages-directory (concat user-emacs-directory "languages/")
          "Directory containing languages configurations")

(defconst etc-directory (concat user-emacs-directory "etc/")
          "Directory containing other configs")


(defun load-core ()
  (load (concat core-directory "packages"))
  (load (concat core-directory "files")))

(defun core-init ()
  (load-core)
  (load-directory packages-directory)
  (load-directory languages-directory)
  (load-directory etc-directory)
  )
